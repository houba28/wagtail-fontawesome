===================
wagtail-fontawesome
===================

wagtail-fontawesome is a `Django <https://www.djangoproject.com>`_ / `Wagtail <https://wagtail.io/>`_ app that provides a couple of Fontawesome/Django/Wagtail related utilities, namely:

- an ``IconField`` to associate Fontawesome icons with model instances
- an ``FAIconBlock`` to associate Fontawesome icons with Wagtail blocks
- templatetag to render css link

also included:

- admin support for the ``IconField``
- fr locale translation


Requirements
============

- PyYAML
- Select2 (included)
- JQuery (uses django's jquery in admin panel)


Settings
========
By default, wagtail-fontawesome ships with and uses the lastest fontawesome release.
You can configure wagtail-fontawesome to use another release/source/cdn by specifying::

    # default uses locally shipped version at 'fontawesome/css/font-awesome.min.css'
    FONTAWESOME_CSS_URL = '//cdn.example.com/fontawesome-min.css'  # absolute url
    FONTAWESOME_CSS_URL = 'myapp/css/fontawesome.min.css'  # relative url

With newer verisons of fontawesome you need to set up integrity hash parameter::

    FONTAWESOME_INTEGRITY_HASH = 'RandomShaHashFromFONTawesome.com/start'

You can also tell it the fontawesome prefix, which as of right now is 'fa', using::

    # fontawesome prefix was deprecated due to FA v5 which has by default 'fas' but also another,
     which can hide possibilites


Installation / Usage
====================

0. Install via pip::

    pip install git+https://bitbucket.org/houba28/django-fontawesome/

    # pip install django-fontawesome is deprecated due to fork

1. Add 'fontawesome' to your installed apps setting like this::

    INSTALLED_APPS = (
        ...
        'fontawesome',
    )

2. Import and use the ``IconField``::
    
    from fontawesome.fields import IconField


    class Category(models.Model):
        ...
        icon = IconField()

3. Or Import and use the ``FAIconBlock``::

    from fontawesome.models import FAIconBlock


    class Category(blocks.StructBlock):
        ...
        icon = FAIconBlock()



Here's what the widget looks like in the admin panel:

|admin-widget|

3. You can then render the icon in your template like this::
    
    {% for category in categories.all %}
        {% if category.icon %}
            {{ category.icon.as_html }}
        {% endif %}
    {% endfor %}


4. wagtail-fontawesome ships with two template tags, ``fontawesome_stylesheet`` and ``fontawesome_icon``.
    - the former inserts a stylesheet link with a pre-configured href according to the ``FONTAWESOME_CSS_URL`` setting
    - the latter renders icons, and accepts the following optional keywords arguments: large, spin, fixed, li, border: (true/false), rotate: (90/180/270), title: (string)
    - you can also colorize an icon using the ``color='red'`` keyword argument to the ``fontawesome_icon`` template tag

    - example usage::

         {% load fontawesome %}
      
         <head>
           {% fontawesome_stylesheet %} 
           ...
         </head>
       
         {% fontawesome_icon 'user' color='red' %}

         {% fontawesome_icon 'star' large=True spin=True %}
      
         <ul class="fa-ul">
            <li> {% fontawesome_icon 'home' rotate=90 li=True %} One</li>
         </ul>


5. profit!!!

.. |admin-widget| image:: docs/images/admin-widget.png

changelog
=========

1.01 - Dec 9, 2018
--------------------
- by Houba28
- added functions for Wagtail
- switched to FontAwesome 5


1.0 - May 10, 2018
--------------------
- django 1.11 compability
- usage of yaml's `safe_load` instead of insecure `load`
- new pypi release (1.0), is now considered a stable release

0.3.1 - Dec 19, 2016
--------------------
- added unicode literals import for cases where icon title attribute uses non ascii chars


Nov 28, 2016
------------
- now suppots django 1.8+
- python3 supported, supposedly
- relative import issues fixed
- updated icon mapping for fontawesome 4.7
- tabular and stacked inlines now supported
- other small fixes
- new PyPI release (0.3)

Jan 28, 2016
------------
- updated icon mapping file (icons.yml) for fontawesome 4.5.0
- new keyword arg for fontawesome_icon template tag: title

Dec 17, 2015
------------
- Updated locally shipped fontawesome to 4.5.0
- fontawesome_icon's output is now marked safe

Sep 11, 2015
------------
- Updated locally shipped fontawesome to 4.4.0

Feb 27, 2015
------------
- added two new keyword argument to the fontawesome_icon template tag, color and border
- FONTAWESOME_PREFIX setting is now taken into account when rendering icons using the fontawesome_icon template tag


Origin
======
Original Author: @Redoudane

website: https://github.com/redouane/django-fontawesome/
Thank you for your work mister.
for more information see License file

