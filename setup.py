import os
from setuptools import setup

README = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='wagtail-fontawesome',
    version='1.1',
    packages=['fontawesome'],
    include_package_data=True,
    license='BSD License',
    description='a django app that provides a couple of fontawesome/wagtail related utilities.',
    long_description=README,
    url='https://bitbucket.org/houba28/wagtail-fontawesome/',
    author="Houba28, based on Redouane Zait's work",
    author_email='houba28@gmail.com',
    install_requires=['PyYAML'],
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django/Wagtail',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Utilities',
    ],
)
