import os
import yaml

PATH = os.path.join(os.path.dirname(__file__), 'icons.yml')


def get_icon_choices():

    choices = [('', '----------')]

    with open(PATH) as f:
        icons = yaml.safe_load(f)

    for icon, values in icons.items():
        for style in values.get('styles'):
            html = "fa" + style[0] + " fa-" + icon
            choices.append((
                html,
                values.get('label')
            ))

    return choices
