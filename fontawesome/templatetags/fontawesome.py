from __future__ import unicode_literals

from django import template
from django.conf import settings
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.utils.html import format_html, mark_safe

register = template.Library()


@register.simple_tag
def fontawesome_stylesheet():
    href = getattr(settings, 'FONTAWESOME_CSS_URL', static('fontawesome/css/fontawesome.min.css'))
    integrity = getattr(settings, 'FONTAWESOME_INTEGRITY_HASH', '')
    if href.endswith('.js'):
        link_body = '<script defer src="{0}"></script>'
    else:
        link_body = '<link rel="stylesheet" href="{0}" integrity="{1}" crossorigin="anonymous">'
    link = format_html(link_body, href, integrity)
    return link
