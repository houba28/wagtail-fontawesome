from django.conf import settings
from .forms import IconFormField
from wagtail.core import blocks
from django import forms

try:
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules([], ["^fontawesome\.fields\.IconField"])
except ImportError:
    pass


class FAIconBlock(blocks.FieldBlock):
    def __init__(self, required=True, help_text=None, **kwargs):
        self.field = IconFormField(help_text=help_text, required=required)
        super().__init__(**kwargs)

    def js_initializer(self):
        return "PrepareIcons()"

    @property
    def media(self):
        # need to pull in StructBlock's own js code as well as our own
        return super().media + forms.Media(
            js=(
                'fontawesome/js/django_fontawesome.js',
                'fontawesome/select2/select2.min.js'
            ),

            css={
                'all': (
                    'fontawesome/css/all.min.css',
                    'fontawesome/select2/select2.css',
                    'fontawesome/select2/select2-bootstrap.css'
                )
            }
        )